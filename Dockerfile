from tomcat:8.0
maintainer Alex Davies
copy target/*.war /opt/tomcat/webapps/hello-dino.war 
expose 8080 
CMD ["catalina.sh", "run"]
